import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class InsertionSortSortingAlgorithm<T extends Comparable<T>> extends SortingAlgorithm<T> {
    private final List<T> boardList;
    private final String colorList;

    public List<T> getBoardList() {
        return boardList;
    }

    public String getColorList() {
        return colorList;
    }

    public InsertionSortSortingAlgorithm(List<T> values, Parameter speed, List<T> boardList, String colorList) {
        super(values, speed);
        this.boardList = boardList;
        this.colorList = colorList;
    }


    @Override
    public void ordered() throws InterruptedException {
        int speedInt = Integer.parseInt(speed.getParameterValue());
        int iteraciones = 1;
        int totalTime;
        List<T> list = getBoardList();
        List<ChessPiece> sortedPiecesByCharacter = Arrays.asList(ChessPiece.values());
        sortedPiecesByCharacter.sort(Comparator.comparing(ChessPiece::getCharacterToPrint));
        List<T> orderedListByCharacter = new ArrayList<>();
        for (ChessPiece piece : sortedPiecesByCharacter) {
            orderedListByCharacter.add((T) piece);
        }
        int n = list.size();
        for (int i = 1; i < n; ++i) {
            iteraciones++;
            T key = list.get(i);
            T keyPrint = orderedListByCharacter.get(i);
            int j = i - 1;
            while (j >= 0 && list.get(j).compareTo(key) > 0) {
                System.out.println("/*/*/*/*/*New board/*/**//**/");
                list.set(j + 1, list.get(j));
                orderedListByCharacter.set(j + 1, orderedListByCharacter.get(j));
                j = j - 1;
                for (T T : orderedListByCharacter){
                    System.out.println(T);
                }
                TimeUnit.MILLISECONDS.sleep(speedInt);
            }
            list.set(j + 1, key);
            orderedListByCharacter.set(j + 1, keyPrint);
        }
        System.out.println("/*/*/*/*/*Final board/*/**//**/");
        totalTime = iteraciones * speedInt;
        System.out.println("Tiempo total: " + totalTime + "ms");
        System.out.println(list);
        if (getColorList().equals("b")) {
            for (T T : orderedListByCharacter) {
                System.out.println(T);
                if (T == ChessPiece.TORRE_II) {
                    System.out.println("\n");
                }
            }
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
        }else {
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
            for (T T : orderedListByCharacter) {
                System.out.println(T);
                if (T == ChessPiece.PEON_8) {
                    System.out.println("\n");
                }
            }
        }
   }
}
