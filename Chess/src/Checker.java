import java.util.Objects;

public class Checker {
    public Checker() {
        //Empty constructor to instantiate in code
    }
    protected void validateAlgorithm(String parameter, String  value) {
        if (Objects.equals(parameter, "a")) {
            if (!value.equals("s") && !value.equals("b") && !value.equals("i") && !value.equals("m") &&
                    !value.equals("q") && !value.equals("h") && !value.equals("c") && !value.equals("r")) {
                System.out.println("Error, valor invalido para selección de algoritmo");
                System.exit(0);
            } else {
                switch (value) {
                    case "s" -> value = "Selection sort";
                    case "b" -> value = "Bubble sort";
                    case "i" -> value = "Insertion sort";
                    case "m" -> value = "Merge sort";
                    case "q" -> value = "Quick sort";
                    case "h" -> value = "Heap sort";
                    case "c" -> value = "Counting sort";
                    case "r" -> value = "Radix sort";
                    default -> value = "Algoritmo de ordenamiento no reconocido";
                }
                System.out.println("Algoritmo: " + value);
            }
        }else {
            System.exit(0);
        }
    }
    protected void validateTypeList(String parameter, String  value) {
        if (Objects.equals(parameter, "t")){
            if (!value.equals("c") && !value.equals("n"))
            {
                System.out.println("Error, Tipo no reconocido");
                System.exit(0);
            }
            else
            {
                if (value.equals("c")) {
                    value = "Lista de caracteres";
                } else {
                    value = "Lista numerica";
                }
                System.out.println("Tipo: " + value);
            }
        }else {
            System.exit(0);
        }
    }
    protected void validateColorPiece(String parameter, String  value) {
        if (Objects.equals(parameter, "o")){
            if (!value.equals("b") && !value.equals("w"))
            {
            System.out.println("Error, tipo de Color de piezas no reconocido");
            System.exit(0);
            }
            else
            {
                String colorPiezas;
                if (value.equals("b")) {
                    colorPiezas = "negras";
                } else {
                    colorPiezas = "blancas";
                }
                System.out.println("Color de piezas: " + colorPiezas);
            }
        }else {
            System.exit(0);
        }
    }
    protected void validateSpeed(String parameter, String  value){
        if (Objects.equals(parameter, "s")){
            int valueInt = Integer.parseInt(value);
            if (valueInt < 100 || valueInt > 1000) {
                System.out.println("Valor para pausa de retardo invalido");
                System.exit(0);
            } else {
                System.out.println("Tiempo total: " + valueInt + " milisegundos");
            }
        }
        else {
            System.exit(0);
        }
    }
    protected void validateNumberOfPieces(String parameter, String  value) {
        if (Objects.equals(parameter, "r")) {
            int valueInt = Integer.parseInt(value);
            String piezas;
            if (valueInt == 1) {
                piezas = "Se coloca al Rey";
            } else if (valueInt == 2) {
                piezas = "Se coloca al Rey y la Reina";
            } else if (valueInt == 4) {
                piezas = "Se coloca los Alfiles, Rey y Reina";
            } else if (valueInt == 6) {
                piezas = "Se coloca los Caballos, Alfiles, Rey y Reina";
            } else if (valueInt == 8) {
                piezas = "Se colocan las Torres, Caballos, Alfiles, Rey y Reina";
            } else if (valueInt == 10) {
                piezas = "Se colocan los peones";
            } else if (valueInt == 16) {
                piezas = "Se colocan todas las piezas";
            } else {
                System.out.println("Error cantidad de piezas.");
                System.exit(0);
                return;
            }

            System.out.println("Numero de piezas : " + piezas);
            }
            else{
                System.exit(0);
            }
        }
    protected void validateIn (String parameter, String value){
        if ("in".equals(parameter) && !"r".equals(value)) {
            System.out.println("Error: Tipo de entrada de valores no válido");
            System.exit(0);
        }
    }

    public void validateParameter(String parameter, String  value) {
        switch (parameter) {
            case "a" -> validateAlgorithm(parameter, value);
            case "t" -> validateTypeList(parameter, value);
            case "o" -> validateColorPiece(parameter,value);
            case "in" -> validateIn(parameter,value);
            case "s" -> validateSpeed(parameter,value);
            case "r" -> validateNumberOfPieces(parameter,value);
            default -> System.exit(0);
        }
    }
}


