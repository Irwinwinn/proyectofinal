# **D.7 Laboratorio Semana 7**

El algoritmo de Ordenamiento implementado es "Insertion sort"

Capturas de pruebas 

|----------------------------------------|-------------------------------------------|
| Ejemplo de entrada                     | Ejemplo de salida                         |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| java CHESS a=i t=n o=b in=r r=16 s=140 |                                           |
|                                        |   ![img.png](Imagenes/test1.png)          |
|  Se oculta la imprecion                |                                           |
|  en cada iteracion que hace el         |                                           |
|  algoritmo de ordenamiento             |                                           |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| java CHESS a=i t=n o=b in=r r=16 s=140 |                                           |
|                                        |   ![img.png](Imagenes/test2-1.png)        |
|  Se realiza la imprecion               |   ![img.png](Imagenes/test2-2.png)        |
|  en cada iteracion que hace el         |                                           |
|  algoritmo de ordenamiento             |                                           |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| java CHESS a=i t=c o=i in=r r=8 s=240  |                                           |
|                                        |   ![img.png](Imagenes/test3.png)          |
|  Se oculta la imprecion                |                                           |
|  en cada iteracion que hace el         |                                           |
|  algoritmo de ordenamiento             |                                           |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| java CHESS a=i t=c o=i in=r r=8 s=240  |                                           |
|                                        |   ![img.png](Imagenes/test4-1.png)        |
|  Se realiza la imprecion               |   ![img.png](Imagenes/test4-2.png)        |
|  en cada iteracion que hace el         |                                           |
|  algoritmo de ordenamiento             |                                           |
|----------------------------------------|-------------------------------------------|

----------------------------------------------------------------------------------------------
# **D.8 Laboratorio Semana 8**


|----------------------------------------|-------------------------------------------|
| Ejemplo de entrada                     | Ejemplo de salida                         |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| java CHESS a=i t=n o=b in=r r=16 s=140 |                                           |
|                                        |   ![img.png](Imagenes/test1Week8.png)     |
|  El algoritmo ejecuta por ejemplo 10   |   ![img.png](Imagenes/test1-5Week8.png)   |
|  iteraciones, por tanto                |                                           |
|  10 * 140 = 1400                       |                                           |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| java CHESS a=i t=c o=w in=r r=8 s=300  |                                           |
|                                        |   ![img.png](Imagenes/test2Week8.png)     |
|  Se realiza la imprecion               |                                           |
|  en cada iteracion que hace el         |                                           |
|  algoritmo de ordenamiento             |                                           |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| Parámetro "a", solo debe aceptar 3     |                                           |
| valores definidos por el               |   ![img.png](Imagenes/error1.png)         |
| programa.                              |                                           |
| Java CHESS a=1 t=n o=b in=r r=16 s=140 |                                           |
|                                        |                                           |
|----------------------------------------|-------------------------------------------|
|                                        |                                           |
| Parámetro "a", solo debe aceptar 3     |                                           |
| valores definidos por el               |   ![img.png](Imagenes/error2.png)         |
| programa.                              |                                           |
| java CHESS t=n o=b in=r r=16 s=140     |                                           |
|                                        |                                           |
|----------------------------------------|-------------------------------------------|
