import java.util.List;

public class AlgorithmSelector {
    Parameter algorithm;
    Parameter listType;
    Parameter color;
    Parameter speed;
    Parameter pieces;
    Parameter in;

    public AlgorithmSelector() {
    }

    public AlgorithmSelector(Parameter algorithm, Parameter listType, Parameter color, Parameter speed, Parameter pieces, Parameter in) {
        this.algorithm = algorithm;
        this.listType = listType;
        this.color = color;
        this.speed = speed;
        this.pieces = pieces;
        this.in = in;
    }

    public void selectAlgorithm(Parameter listType, Parameter algorithm, Parameter speed, Parameter pieces, Parameter color) throws InterruptedException {
        if ("c".equals(listType.getParameterValue())) {
            CreateList<String> charactersList = new CreateList<>(algorithm.getParameterValue());
            List<String> characterList = charactersList.generateCharacterListFromChessPieces(Integer.parseInt(pieces.getParameterValue()));
            List<String> characterListToOrder = charactersList.generateCharacterListOnBoard(Integer.parseInt(pieces.getParameterValue()), color.getParameterValue());
            System.out.println("Lista inicial:");
            System.out.println(characterListToOrder);


            if ("i".equals(algorithm.getParameterValue())) {
                InsertionSortSortingAlgorithm<String> insertionSortSortingAlgorithm = new InsertionSortSortingAlgorithm<>(characterList, speed,characterListToOrder, color.getParameterValue());
                insertionSortSortingAlgorithm.ordered();
            } else if ("s".equals(algorithm.getParameterValue())) {
                SelectionSortSortingAlgorithm<String> selectionSortSortingAlgorithm = new SelectionSortSortingAlgorithm<>(characterList, speed,characterListToOrder, color.getParameterValue());
                selectionSortSortingAlgorithm.ordered();
            } else if ("b".equals(algorithm.getParameterValue())) {
                BubbleSortSortingAlgorithm<String> bubbleSortSortingAlgorithm = new BubbleSortSortingAlgorithm<>(characterList, speed,characterListToOrder, color.getParameterValue());
                bubbleSortSortingAlgorithm.ordered();
            } else if ("q".equals(algorithm.getParameterValue())) {
                QuickSortSortingAlgorithm<String> quickSortSortingAlgorithm = new QuickSortSortingAlgorithm<>(characterList, speed,characterListToOrder, color.getParameterValue());
                quickSortSortingAlgorithm.ordered();
            } else {
                System.out.println("Error, no se ah implementado este algoritmo");
            }
        } else if ("n".equals(listType.getParameterValue())) {
            CreateList<Integer> numericList = new CreateList<>(color.getParameterValue());
            List<Integer> numberList = numericList.generateCharacterListFromChessPieces(Integer.parseInt(pieces.getParameterValue()));
            List<Integer> numberListToOrder = numericList.generateNumberListOnBoard(Integer.parseInt(pieces.getParameterValue()),color.getParameterValue());
            System.out.println("Valores:");
            System.out.println(numberListToOrder);

            if ("i".equals(algorithm.getParameterValue())) {
                InsertionSortSortingAlgorithm<Integer> insertionSortSortingAlgorithm = new InsertionSortSortingAlgorithm<>(numberList, speed,numberListToOrder, color.getParameterValue());
                insertionSortSortingAlgorithm.ordered();
            } else if ("s".equals(algorithm.getParameterValue())) {
                SelectionSortSortingAlgorithm<Integer> selectionSortSortingAlgorithm = new SelectionSortSortingAlgorithm<>(numberList, speed,numberListToOrder, color.getParameterValue());
                selectionSortSortingAlgorithm.ordered();
            } else if ("b".equals(algorithm.getParameterValue())) {
                BubbleSortSortingAlgorithm<Integer> bubbleSortSortingAlgorithm = new BubbleSortSortingAlgorithm<>(numberList, speed,numberListToOrder, color.getParameterValue());
                bubbleSortSortingAlgorithm.ordered();
            } else if ("q".equals(algorithm.getParameterValue())) {
                QuickSortSortingAlgorithm<Integer> quickSortSortingAlgorithm = new QuickSortSortingAlgorithm<>(numberList, speed,numberListToOrder, color.getParameterValue());
                quickSortSortingAlgorithm.ordered();
            } else {
                System.out.println("Error, no se ah implementado este algoritmo");
            }
        }
    }
}
