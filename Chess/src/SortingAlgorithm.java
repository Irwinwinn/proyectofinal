import java.util.List;
public abstract class SortingAlgorithm<T> {
   protected List<T> values;
   protected Parameter speed;

   protected SortingAlgorithm() {
   }

   public List<T> getValues() {
      return values;
   }

   public void setValues(List<T> values) {
      this.values = values;
   }

   public Parameter getSpeed() {
      return speed;
   }

   public void setSpeed(Parameter speed) {
      this.speed = speed;
   }

   protected SortingAlgorithm(List<T> values, Parameter speed) {
      this.values = values;
      this.speed = speed;
   }

   protected abstract void ordered() throws InterruptedException;


}


