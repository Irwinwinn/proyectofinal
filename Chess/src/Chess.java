public class Chess {
    public static void main(String[] args) throws InterruptedException
    {
        ArgumentProcessor.processArguments(args);

        Parameter algorithm = new Parameter("algorithm",ArgumentProcessor.getAlgorithmValue());
        Parameter listType = new Parameter("listType",ArgumentProcessor.getListTypeValue());
        Parameter color = new Parameter("color",ArgumentProcessor.getColorValue());
        Parameter speed = new Parameter("speed",ArgumentProcessor.getSpeedValue());
        Parameter pieces = new Parameter("pieces",ArgumentProcessor.getPiecesValue());

        AlgorithmSelector algorithmSelector = new AlgorithmSelector();
        algorithmSelector.selectAlgorithm(listType,algorithm,speed,pieces,color);
    }
}