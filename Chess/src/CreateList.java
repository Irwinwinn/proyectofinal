import java.util.*;
import java.util.function.Consumer;

public class CreateList < T > implements Iterable < T > {
    private List< T > pieces;
    private String colorOfPieces = "";

    public String getColorOfPieces() {
        return colorOfPieces;
    }

    public CreateList(String colorOfPieces) {
        this.colorOfPieces = colorOfPieces;
    }

    public void setColorOfPieces(String colorOfPieces) {
        this.colorOfPieces = colorOfPieces;
    }

    public CreateList(List<Comparable<?>> pieces, String colorOfPieces) {
        this.pieces = (List<T>) pieces;
        this.colorOfPieces = colorOfPieces;
    }

    public CreateList() {
    }

    public List< T > getPieces() {
        return pieces;
    }
    //Caracteres
    public List<T> generateCharacterListFromChessPieces(int numberOfPieces)
    {
        List<String> characterList = new ArrayList<>();
        switch (numberOfPieces) {
            case 1 -> characterList.add(ChessPiece.REY.getName());
            case 2 -> {
                characterList.add(ChessPiece.REY.getName());
                characterList.add(ChessPiece.REINA.getName());
            }
            case 4 -> {
                characterList.add(ChessPiece.REY.getName());
                characterList.add(ChessPiece.REINA.getName());
                characterList.add(ChessPiece.ALFIL_I.getName());
                characterList.add(ChessPiece.ALFIL_II.getName());
            }
            case 6 -> {
                characterList.add(ChessPiece.REY.getName());
                characterList.add(ChessPiece.REINA.getName());
                characterList.add(ChessPiece.ALFIL_I.getName());
                characterList.add(ChessPiece.ALFIL_II.getName());
                characterList.add(ChessPiece.CABALLO_I.getName());
                characterList.add(ChessPiece.CABALLO_II.getName());
            }
            case 8 -> {
                characterList.add(ChessPiece.REY.getName());
                characterList.add(ChessPiece.REINA.getName());
                characterList.add(ChessPiece.ALFIL_I.getName());
                characterList.add(ChessPiece.ALFIL_II.getName());
                characterList.add(ChessPiece.CABALLO_I.getName());
                characterList.add(ChessPiece.CABALLO_II.getName());
                characterList.add(ChessPiece.TORRE_I.getName());
                characterList.add(ChessPiece.TORRE_II.getName());
            }
            case 10 -> {
                for (int i = 1; i <= 8; i++) {
                    characterList.add(ChessPiece.valueOf("PEON_" + i).getName());
                }
            }
            case 16 -> {
                for (ChessPiece piece : ChessPiece.values()) {
                    characterList.add(piece.getName());
                }
            }
            default -> throw new IllegalArgumentException("Número de piezas no válido: " + numberOfPieces);
        }
        Collections.shuffle(characterList);
        return (List<T>) characterList;
    }
    public List<T> generateCharacterListOnBoard(int numberOfPieces,String colorOfPieces){
        List<String> characterList = new ArrayList<>();
        if (colorOfPieces.equals("b")){
            blackCharacterList(numberOfPieces,characterList);
        }else if (colorOfPieces.equals("w")){
            whiteCharacterList(numberOfPieces,characterList);
        }
        return (List<T>) characterList;
    }
    public void blackCharacterList(int numberOfPieces, List<String> blackCharacterList){
        switch (numberOfPieces) {
            case 1 -> blackCharacterList.add(ChessPiece.REY.getBlackCharacterOnBoard());
            case 2 -> {
                blackCharacterList.add(ChessPiece.REY.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.REINA.getBlackCharacterOnBoard());
            }
            case 4 -> {
                blackCharacterList.add(ChessPiece.REY.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.REINA.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.ALFIL_I.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.ALFIL_II.getBlackCharacterOnBoard());
            }
            case 6 -> {
                blackCharacterList.add(ChessPiece.REY.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.REINA.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.ALFIL_I.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.ALFIL_II.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.CABALLO_I.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.CABALLO_II.getBlackCharacterOnBoard());
            }
            case 8 -> {
                blackCharacterList.add(ChessPiece.REY.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.REINA.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.ALFIL_I.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.ALFIL_II.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.CABALLO_I.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.CABALLO_II.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.TORRE_I.getBlackCharacterOnBoard());
                blackCharacterList.add(ChessPiece.TORRE_II.getBlackCharacterOnBoard());
            }
            case 10 -> {
                for (int i = 1; i <= 8; i++) {
                    blackCharacterList.add(ChessPiece.valueOf("PEON_" + i).getBlackCharacterOnBoard());
                }
            }
            case 16 -> {
                for (ChessPiece piece : ChessPiece.values()) {
                    blackCharacterList.add(piece.getBlackCharacterOnBoard());
                }
            }
            default -> throw new IllegalArgumentException("Número de piezas no válido: " + numberOfPieces);
        }
    }
    public void whiteCharacterList(int numberOfPieces, List<String> whiteCharacterList){
        switch (numberOfPieces) {
            case 1 -> whiteCharacterList.add(ChessPiece.REY.getWhiteCharacterOnBoard());
            case 2 -> {
                whiteCharacterList.add(ChessPiece.REY.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.REINA.getWhiteCharacterOnBoard());
            }
            case 4 -> {
                whiteCharacterList.add(ChessPiece.REY.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.REINA.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.ALFIL_I.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.ALFIL_II.getWhiteCharacterOnBoard());
            }
            case 6 -> {
                whiteCharacterList.add(ChessPiece.REY.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.REINA.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.ALFIL_I.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.ALFIL_II.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.CABALLO_I.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.CABALLO_II.getWhiteCharacterOnBoard());
            }
            case 8 -> {
                whiteCharacterList.add(ChessPiece.REY.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.REINA.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.ALFIL_I.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.ALFIL_II.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.CABALLO_I.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.CABALLO_II.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.TORRE_I.getWhiteCharacterOnBoard());
                whiteCharacterList.add(ChessPiece.TORRE_II.getWhiteCharacterOnBoard());
            }
            case 10 -> {
                for (int i = 1; i <= 8; i++) {
                    whiteCharacterList.add(ChessPiece.valueOf("PEON_" + i).getWhiteCharacterOnBoard());
                }
            }
            case 16 -> {
                for (ChessPiece piece : ChessPiece.values()) {
                    whiteCharacterList.add(piece.getWhiteCharacterOnBoard());
                }
            }
            default -> throw new IllegalArgumentException("Número de piezas no válido: " + numberOfPieces);
        }
    }

    ////numeros
    public List<Integer> generateNumberListFromChessPieces(int numberOfPieces)
    {
        List<Integer> numberList = new ArrayList<>();
        switch (numberOfPieces) {
            case 1 -> numberList.add(ChessPiece.REY.getNumberToPrint());
            case 2 -> {
                numberList.add(ChessPiece.REY.getNumberToPrint());
                numberList.add(ChessPiece.REINA.getNumberToPrint());
            }
            case 4 -> {
                numberList.add(ChessPiece.REY.getNumberToPrint());
                numberList.add(ChessPiece.REINA.getNumberToPrint());
                numberList.add(ChessPiece.ALFIL_I.getNumberToPrint());
                numberList.add(ChessPiece.ALFIL_II.getNumberToPrint());
            }
            case 6 -> {
                numberList.add(ChessPiece.REY.getNumberToPrint());
                numberList.add(ChessPiece.REINA.getNumberToPrint());
                numberList.add(ChessPiece.ALFIL_I.getNumberToPrint());
                numberList.add(ChessPiece.ALFIL_II.getNumberToPrint());
                numberList.add(ChessPiece.CABALLO_I.getNumberToPrint());
                numberList.add(ChessPiece.CABALLO_II.getNumberToPrint());
            }
            case 8 -> {
                numberList.add(ChessPiece.REY.getNumberToPrint());
                numberList.add(ChessPiece.REINA.getNumberToPrint());
                numberList.add(ChessPiece.ALFIL_I.getNumberToPrint());
                numberList.add(ChessPiece.ALFIL_II.getNumberToPrint());
                numberList.add(ChessPiece.CABALLO_I.getNumberToPrint());
                numberList.add(ChessPiece.CABALLO_II.getNumberToPrint());
                numberList.add(ChessPiece.TORRE_I.getNumberToPrint());
                numberList.add(ChessPiece.TORRE_II.getNumberToPrint());
            }
            case 10 -> {
                for (int i = 1; i <= 8; i++) {
                    numberList.add(ChessPiece.valueOf("PEON_" + i).getNumberToPrint());
                }
            }
            case 16 -> {
                for (ChessPiece piece : ChessPiece.values()) {
                    numberList.add(piece.getNumberToPrint());
                }
            }
            default -> throw new IllegalArgumentException("Número de piezas no válido: " + numberOfPieces);
        }
        Collections.shuffle(numberList);
        return numberList;
    }
    public List<T> generateNumberListOnBoard(int numberOfPieces,String colorOfPieces){
        List<Integer> numberList = new ArrayList<>();
        if (colorOfPieces.equals("b")){
            blackNumberList(numberOfPieces,numberList);
        }else if (colorOfPieces.equals("w")){
            whiteNumberList(numberOfPieces,numberList);
        }
        return (List<T>) numberList;
    }
    public void blackNumberList(int numberOfPieces, List<Integer> blackNumberList){
        switch (numberOfPieces) {
            case 1 -> blackNumberList.add(ChessPiece.REY.getBlackNumberOnBoard());
            case 2 -> {
                blackNumberList.add(ChessPiece.REY.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getBlackNumberOnBoard());
            }
            case 4 -> {
                blackNumberList.add(ChessPiece.REY.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_I.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_II.getBlackNumberOnBoard());
            }
            case 6 -> {
                blackNumberList.add(ChessPiece.REY.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_I.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_II.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_I.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_II.getBlackNumberOnBoard());
            }
            case 8 -> {
                blackNumberList.add(ChessPiece.REY.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_I.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_II.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_I.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_II.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.TORRE_I.getBlackNumberOnBoard());
                blackNumberList.add(ChessPiece.TORRE_II.getBlackNumberOnBoard());
            }
            case 10 -> {
                for (int i = 1; i <= 8; i++) {
                    blackNumberList.add(ChessPiece.valueOf("PEON_" + i).getBlackNumberOnBoard());
                }
            }
            case 16 -> {
                for (ChessPiece piece : ChessPiece.values()) {
                    blackNumberList.add(piece.getBlackNumberOnBoard());
                }
            }
            default -> throw new IllegalArgumentException("Número de piezas no válido: " + numberOfPieces);
        }
    }
    public void whiteNumberList(int numberOfPieces, List<Integer> blackNumberList){
        switch (numberOfPieces) {
            case 1 -> blackNumberList.add(ChessPiece.REY.getWhiteNumberOnBoard());
            case 2 -> {
                blackNumberList.add(ChessPiece.REY.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getWhiteNumberOnBoard());
            }
            case 4 -> {
                blackNumberList.add(ChessPiece.REY.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_I.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_II.getWhiteNumberOnBoard());
            }
            case 6 -> {
                blackNumberList.add(ChessPiece.REY.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_I.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_II.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_I.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_II.getWhiteNumberOnBoard());
            }
            case 8 -> {
                blackNumberList.add(ChessPiece.REY.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.REINA.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_I.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.ALFIL_II.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_I.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.CABALLO_II.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.TORRE_I.getWhiteNumberOnBoard());
                blackNumberList.add(ChessPiece.TORRE_II.getWhiteNumberOnBoard());
            }
            case 10 -> {
                for (int i = 1; i <= 8; i++) {
                    blackNumberList.add(ChessPiece.valueOf("PEON_" + i).getWhiteNumberOnBoard());
                }
            }
            case 16 -> {
                for (ChessPiece piece : ChessPiece.values()) {
                    blackNumberList.add(piece.getWhiteNumberOnBoard());
                }
            }
            default -> throw new IllegalArgumentException("Número de piezas no válido: " + numberOfPieces);
        }
    }



    public void setPieces(List<Comparable<?>> pieces) {
        this.pieces = (List<T>) pieces;
    }



    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        Iterable.super.forEach(action);
    }

    @Override
    public Spliterator<T> spliterator() {
        return Iterable.super.spliterator();
    }
}
