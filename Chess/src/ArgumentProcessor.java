public class ArgumentProcessor {
    private static String algorithmValue;
    private static String listTypeValue;
    private static String colorValue;
    private static String piecesValue;
    private static String speedValue;
    private static String inValue;

    public static void processArguments(String[] args) {
        Checker validator = new Checker();
        boolean algorithmProvided = false;
        boolean listTypeProvided = false;
        boolean colorProvided = false;
        boolean piecesProvided = false;
        boolean speedProvided = false;
        boolean inProvided = false;

        for (String arg : args) {
            String[] parts = arg.split("=");
            if (parts.length == 2) {
                String key = parts[0];
                String value = parts[1];
                validator.validateParameter(key, value.toLowerCase());

                switch (key) {
                    case "a" -> {
                        algorithmValue = value;
                        algorithmProvided = true;
                    }
                    case "t" -> {
                        listTypeValue = value;
                        listTypeProvided = true;
                    }
                    case "o" -> {
                        colorValue = value;
                        colorProvided = true;
                    }
                    case "r" -> {
                        piecesValue = value;
                        piecesProvided = true;
                    }
                    case "s" -> {
                        speedValue = value;
                        speedProvided = true;
                    }
                    case "in" -> {
                        inValue = value;
                        inProvided = true;
                    }
                    default -> System.out.println("Clave no reconocida: " + key);
                }
            } else {
                System.out.println("Error, valor invalido el argumento '" + arg + "' esta incompleto");
                System.exit(0);
            }
        }
        if (args.length == 0) {
            System.out.println("No se proporcionaron argumentos.");
            System.exit(0);
        }

        if (!algorithmProvided) {
            System.out.println("Error, no existe valor para selección de algoritmo");
            System.exit(0);
        }
        if (!listTypeProvided) {
            System.out.println("Error, falta valor para Tipo");
            System.exit(0);
        }
        if (!colorProvided) {
            System.out.println("Error, falta tipo de ordenamiento");
            System.exit(0);
        }
        if (!piecesProvided) {
            System.out.println("Error, falta cantidad de piezas.");
            System.exit(0);
        }
        if (!speedProvided) {
            System.out.println("Falta valor para pausa de retardo");
            System.exit(0);
        }
        if (!inProvided) {
            System.out.println("Error, falta tipo de entrada de valores");
            System.exit(0);
        }
    }


    public static String getAlgorithmValue() {
        return algorithmValue;
    }

    public static String getListTypeValue() {
        return listTypeValue;
    }

    public static String getColorValue() {
        return colorValue;
    }

    public static String getPiecesValue() {
        return piecesValue;
    }

    public static String getSpeedValue() {
        return speedValue;
    }

}
