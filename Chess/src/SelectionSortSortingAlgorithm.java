import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SelectionSortSortingAlgorithm<T extends Comparable<T>> extends SortingAlgorithm<T> {
    private final List<T> boardList;
    private final String colorList;

    public List<T> getBoardList() {
        return boardList;
    }

    public String getColorList() {
        return colorList;
    }
    public SelectionSortSortingAlgorithm(List<T> values, Parameter speed, List<T> boardList, String colorList) {
        super(values, speed);
        this.boardList = boardList;
        this.colorList = colorList;
    }
    @Override
    public void ordered() throws InterruptedException {
        int speedInt = Integer.parseInt(speed.getParameterValue());
        int iteraciones = 1;
        int totalTime;
        List<T> list = getBoardList();
        List<ChessPiece> sortedPiecesByCharacter = Arrays.asList(ChessPiece.values());
        sortedPiecesByCharacter.sort(Comparator.comparing(ChessPiece::getCharacterToPrint));
        List<T> orderedListByCharacter = new ArrayList<>();
        for (ChessPiece piece : sortedPiecesByCharacter) {
            orderedListByCharacter.add((T) piece);
        }
        int n = list.size();
        for (int i = 0; i < n - 1; i++) {
            System.out.println("/*/*/*/*/*New board/*/**//**/");
            int minIndex = i;
            for (int j = i + 1; j < n; j++) {
                iteraciones++;
                if (list.get(j).compareTo(list.get(minIndex)) < 0) {

                    minIndex = j;
                }
            }
            T temp = list.get(minIndex);
            list.set(minIndex, list.get(i));
            list.set(i, temp);
            T tempPrint = orderedListByCharacter.get(minIndex);
            orderedListByCharacter.set(minIndex, orderedListByCharacter.get(i));
            orderedListByCharacter.set(i,tempPrint);
            for (T item : orderedListByCharacter) {
                System.out.println(item);
            }
            TimeUnit.MILLISECONDS.sleep(speedInt);
        }
        System.out.println("/*/*/*/*/*Final board/*/**//**/");
        totalTime = iteraciones * speedInt;
        System.out.println("Tiempo total: " + totalTime + "ms");
        if (getColorList().equals("b")) {
            for (T item : orderedListByCharacter) {
                System.out.println(item);
                if (item == ChessPiece.TORRE_II) {
                    System.out.println("\n");
                }
            }
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
        } else {
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
            for (T item : orderedListByCharacter) {
                System.out.println(item);
                if (item == ChessPiece.PEON_8) {
                    System.out.println("\n");
                }
            }
        }
    }
}
