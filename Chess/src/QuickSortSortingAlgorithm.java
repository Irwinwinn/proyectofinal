import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class QuickSortSortingAlgorithm<T extends Comparable<T>> extends SortingAlgorithm<T> {
    private final List<T> boardList;
    private final String colorList;

    public List<T> getBoardList() {
        return boardList;
    }

    public String getColorList() {
        return colorList;
    }

    public QuickSortSortingAlgorithm(List<T> values, Parameter speed, List<T> boardList, String colorList) {
        super(values, speed);
        this.boardList = boardList;
        this.colorList = colorList;
    }
    @Override
    public void ordered() throws InterruptedException {
        int speedInt = Integer.parseInt(speed.getParameterValue());
        int totalTime;
        int iteraciones= 0;
        List<T> list = getBoardList();
        List<ChessPiece> sortedPiecesByCharacter = Arrays.asList(ChessPiece.values());
        sortedPiecesByCharacter.sort(Comparator.comparing(ChessPiece::getCharacterToPrint));
        List<T> orderedListByCharacter = new ArrayList<>();
        for (ChessPiece piece : sortedPiecesByCharacter) {
            orderedListByCharacter.add((T) piece);
        }
        quickSort(list, 0, list.size() - 1, orderedListByCharacter);

        totalTime = iteraciones * speedInt;
        System.out.println("Tiempo total: " + totalTime + "ms");
        if (getColorList().equals("b")) {
            for (T item : orderedListByCharacter) {
                System.out.println(item);
                if (item == ChessPiece.TORRE_II) {
                    System.out.println("\n");
                }
            }
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
        } else {
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
            for (T item : orderedListByCharacter) {
                System.out.println(item);
                if (item == ChessPiece.PEON_8) {
                    System.out.println("\n");
                }
            }
        }
    }

    private void quickSort(List<T> list, int low, int high, List<T> orderedListByCharacter) throws InterruptedException {
        if (low < high) {
            int pi = partition(list, low, high, orderedListByCharacter);

            quickSort(list, low, pi - 1, orderedListByCharacter);
            quickSort(list, pi + 1, high, orderedListByCharacter);
        }
    }

    private int partition(List<T> list, int low, int high, List<T> orderedListByCharacter) throws InterruptedException {
        int speedInt = Integer.parseInt(speed.getParameterValue());
        T pivot = list.get(high);
        int i = low - 1;

        for (int j = low; j < high; j++) {
            if (list.get(j).compareTo(pivot) < 0) {
                System.out.println("/*/*/*/*/*New board/*/**//**/");
                i++;
                T temp = list.get(i);
                list.set(i, list.get(j));
                list.set(j, temp);

                temp = orderedListByCharacter.get(i);
                orderedListByCharacter.set(i, orderedListByCharacter.get(j));
                orderedListByCharacter.set(j, temp);

                for (T item : orderedListByCharacter) {
                    System.out.println(item);
                }
                TimeUnit.MILLISECONDS.sleep(speedInt);
            }
        }

        T temp = list.get(i + 1);
        list.set(i + 1, list.get(high));
        list.set(high, temp);

        temp = orderedListByCharacter.get(i + 1);
        orderedListByCharacter.set(i + 1, orderedListByCharacter.get(high));
        orderedListByCharacter.set(high, temp);
        return i + 1;
    }

}
