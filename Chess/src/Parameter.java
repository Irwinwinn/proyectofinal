public class Parameter {
    String parameterName;
    String parameterValue;

    public Parameter(String parameterName, String parameterValue) {
        this.parameterName = parameterName;
        this.parameterValue = parameterValue;
    }

    public String getParameterValue() {
        return parameterValue;
    }

}
