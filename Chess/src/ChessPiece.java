import java.util.List;

public enum ChessPiece {
    REY("Rey", "a", 1, "e", "m", 5, 13),
    REINA("Reina", "b", 2, "d", "l", 4, 12),
    TORRE_I("Torre I", "c", 3, "a", "i", 1, 9),
    TORRE_II("Torre II", "d", 4, "h", "p", 8, 16),
    ALFIL_I("Alfil I", "e", 5, "c", "k", 3, 11),
    ALFIL_II("Alfil II", "f", 6, "f", "n", 6, 14),
    CABALLO_I("Caballo I", "g", 7, "b", "j", 2, 10),
    CABALLO_II("Caballo II", "h", 8, "g", "o", 7, 15),
    PEON_1("Peón 1", "i", 9, "i", "a", 9, 1),
    PEON_2("Peón 2", "j", 10, "j", "b", 10, 2),
    PEON_3("Peón 3", "k", 11, "k", "c", 11, 3),
    PEON_4("Peón 4", "l", 12, "l", "d", 12, 4),
    PEON_5("Peón 5", "m", 13, "m", "e", 13, 5),
    PEON_6("Peón 6", "n", 14, "n", "f", 14, 6),
    PEON_7("Peón 7", "o", 15, "o", "g", 15, 7),
    PEON_8("Peón 8", "p", 16, "p", "h", 16, 8);


    private final String name;
    private final String characterToPrint;
    private final int numberToPrint;
    private final String blackCharacterOnBoard;
    private final String whiteCharacterOnBoard;
    private final int blackNumberOnBoard;
    private final int whiteNumberOnBoard;

    public String getCharacterToPrint() {
        return characterToPrint;
    }

    ChessPiece(String name, String characterToPrint, int numberToPrint, String blackCharacterOnBoard, String whiteCharacterOnBoard, int blackNumberOnBoard, int whiteNumberOnBoard) {
        this.name = name;
        this.characterToPrint = characterToPrint;
        this.numberToPrint = numberToPrint;
        this.blackCharacterOnBoard = blackCharacterOnBoard;
        this.whiteCharacterOnBoard = whiteCharacterOnBoard;
        this.blackNumberOnBoard = blackNumberOnBoard;
        this.whiteNumberOnBoard = whiteNumberOnBoard;
    }

    public String getName() {
        return name;
    }
    public int getNumberToPrint() {
        return numberToPrint;
    }

    public String getBlackCharacterOnBoard() {
        return blackCharacterOnBoard;
    }

    public String getWhiteCharacterOnBoard() {
        return whiteCharacterOnBoard;
    }
    public int getBlackNumberOnBoard() {
        return blackNumberOnBoard;
    }
    public int getWhiteNumberOnBoard() {
        return whiteNumberOnBoard;
    }
    public <T> void generatePrintList(int pieces){
        CreateList<T> pieceList = new CreateList<>();
        List<T> prin = pieceList.generateCharacterListFromChessPieces(pieces);

    }

}
