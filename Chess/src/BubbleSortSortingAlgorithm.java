import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BubbleSortSortingAlgorithm<T extends Comparable<T>> extends SortingAlgorithm<T> {
    private final List<T> boardList;
    private final String colorList;

    public List<T> getBoardList() {
        return boardList;
    }

    public String getColorList() {
        return colorList;
    }

    public BubbleSortSortingAlgorithm(List<T> values, Parameter speed, List<T> boardList, String colorList) {
        super(values, speed);
        this.boardList = boardList;
        this.colorList = colorList;
    }


    @Override
    public List<T> getValues() {
        return super.getValues();
    }
    @Override
    public void ordered() throws InterruptedException {
        int speedInt = Integer.parseInt(speed.getParameterValue());
        int iteraciones = 1;
        int totalTime;
        List<T> list = getBoardList();
        List<ChessPiece> sortedPiecesByCharacter = Arrays.asList(ChessPiece.values());
        sortedPiecesByCharacter.sort(Comparator.comparing(ChessPiece::getCharacterToPrint));
        List<T> orderedListByCharacter = new ArrayList<>();
        for (ChessPiece piece : sortedPiecesByCharacter) {
            orderedListByCharacter.add((T) piece);
        }
        int n = list.size();

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (list.get(j).compareTo(list.get(j + 1)) > 0) {
                    System.out.println("/*/*/*/*/*New board/*/**//**/");
                    T temp = list.get(j);
                    T tempPrint = orderedListByCharacter.get(j);

                    list.set(j, list.get(j + 1));
                    list.set(j + 1, temp);

                    orderedListByCharacter.set(j, orderedListByCharacter.get(j + 1));
                    orderedListByCharacter.set(j + 1, tempPrint);

                    for (T item : orderedListByCharacter) {
                        System.out.println(item);
                    }
                    TimeUnit.MILLISECONDS.sleep(speedInt);
                    iteraciones++;
                }
            }
        }
        System.out.println("/*/*/*/*/*Final board/*/**//**/");
        totalTime = iteraciones * speedInt;
        System.out.println("Tiempo total: " + totalTime + "ms");
        if (getColorList().equals("b")) {
            for (T item : orderedListByCharacter) {
                System.out.println(item);
                if (item == ChessPiece.TORRE_II) {
                    System.out.println("\n");
                }
            }
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
        } else {
            System.out.println("- - - - - - - - \n" + "- - - - - - - - \n" +
                    "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n" + "- - - - - - - - \n");
            for (T item : orderedListByCharacter) {
                System.out.println(item);
                if (item == ChessPiece.PEON_8) {
                    System.out.println("\n");
                }
            }
        }

    }
}
